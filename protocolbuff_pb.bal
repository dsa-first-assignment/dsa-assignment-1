import ballerina/grpc;

public isolated client class functionsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(Adddeveloperfn|ContextAdddeveloperfn req) returns (string|grpc:Error) {
        map<string|string[]> headers = {};
        Adddeveloperfn message;
        if (req is ContextAdddeveloperfn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function add_new_fnContext(Adddeveloperfn|ContextAdddeveloperfn req) returns (ContextString|grpc:Error) {
        map<string|string[]> headers = {};
        Adddeveloperfn message;
        if (req is ContextAdddeveloperfn) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function add_fns(Addmultiplefnsrequest|ContextAddmultiplefnsrequest req) returns (Addmultiplefnsresponse|grpc:Error) {
        map<string|string[]> headers = {};
        Addmultiplefnsrequest message;
        if (req is ContextAddmultiplefnsrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Addmultiplefnsresponse>result;
    }

    isolated remote function add_fnsContext(Addmultiplefnsrequest|ContextAddmultiplefnsrequest req) returns (ContextAddmultiplefnsresponse|grpc:Error) {
        map<string|string[]> headers = {};
        Addmultiplefnsrequest message;
        if (req is ContextAddmultiplefnsrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Addmultiplefnsresponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(deletefnrequest|ContextDeletefnrequest req) returns (deletefnresponse|grpc:Error) {
        map<string|string[]> headers = {};
        deletefnrequest message;
        if (req is ContextDeletefnrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <deletefnresponse>result;
    }

    isolated remote function delete_fnContext(deletefnrequest|ContextDeletefnrequest req) returns (ContextDeletefnresponse|grpc:Error) {
        map<string|string[]> headers = {};
        deletefnrequest message;
        if (req is ContextDeletefnrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <deletefnresponse>result, headers: respHeaders};
    }

    isolated remote function show_fn(showfnrequest|ContextShowfnrequest req) returns stream<showfnResponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        showfnrequest message;
        if (req is ContextShowfnrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_fn", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        ShowfnResponseStream outputStream = new ShowfnResponseStream(result);
        return new stream<showfnResponse, grpc:Error?>(outputStream);
    }

    isolated remote function show_fnContext(showfnrequest|ContextShowfnrequest req) returns ContextShowfnResponseStream|grpc:Error {
        map<string|string[]> headers = {};
        showfnrequest message;
        if (req is ContextShowfnrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_fn", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        ShowfnResponseStream outputStream = new ShowfnResponseStream(result);
        return {content: new stream<showfnResponse, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_fns(showallfnsrequest|ContextShowallfnsrequest req) returns stream<showallfnsresponseParams, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        showallfnsrequest message;
        if (req is ContextShowallfnsrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        ShowallfnsresponseParamsStream outputStream = new ShowallfnsresponseParamsStream(result);
        return new stream<showallfnsresponseParams, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(showallfnsrequest|ContextShowallfnsrequest req) returns ContextShowallfnsresponseParamsStream|grpc:Error {
        map<string|string[]> headers = {};
        showallfnsrequest message;
        if (req is ContextShowallfnsrequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        ShowallfnsresponseParamsStream outputStream = new ShowallfnsresponseParamsStream(result);
        return {content: new stream<showallfnsresponseParams, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(showallwithcriteriarequest|ContextShowallwithcriteriarequest req) returns stream<showallwithcriteriaresponse, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        showallwithcriteriarequest message;
        if (req is ContextShowallwithcriteriarequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_with_criteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        ShowallwithcriteriaresponseStream outputStream = new ShowallwithcriteriaresponseStream(result);
        return new stream<showallwithcriteriaresponse, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_with_criteriaContext(showallwithcriteriarequest|ContextShowallwithcriteriarequest req) returns ContextShowallwithcriteriaresponseStream|grpc:Error {
        map<string|string[]> headers = {};
        showallwithcriteriarequest message;
        if (req is ContextShowallwithcriteriarequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_with_criteria", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        ShowallwithcriteriaresponseStream outputStream = new ShowallwithcriteriaresponseStream(result);
        return {content: new stream<showallwithcriteriaresponse, grpc:Error?>(outputStream), headers: respHeaders};
    }
}

public class ShowfnResponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|showfnResponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|showfnResponse value;|} nextRecord = {value: <showfnResponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public class ShowallfnsresponseParamsStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|showallfnsresponseParams value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|showallfnsresponseParams value;|} nextRecord = {value: <showallfnsresponseParams>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public class ShowallwithcriteriaresponseStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|showallwithcriteriaresponse value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|showallwithcriteriaresponse value;|} nextRecord = {value: <showallwithcriteriaresponse>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class FunctionsAddmultiplefnsresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAddmultiplefnsresponse(Addmultiplefnsresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAddmultiplefnsresponse(ContextAddmultiplefnsresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsShowallwithcriteriaresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowallwithcriteriaresponse(showallwithcriteriaresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowallwithcriteriaresponse(ContextShowallwithcriteriaresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsDeletefnresponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendDeletefnresponse(deletefnresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextDeletefnresponse(ContextDeletefnresponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsShowallfnsresponseParamsCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowallfnsresponseParams(showallfnsresponseParams response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowallfnsresponseParams(ContextShowallfnsresponseParams response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsShowfnResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowfnResponse(showfnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowfnResponse(ContextShowfnResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextShowallwithcriteriaresponseStream record {|
    stream<showallwithcriteriaresponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowfnResponseStream record {|
    stream<showfnResponse, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowallfnsresponseParamsStream record {|
    stream<showallfnsresponseParams, error?> content;
    map<string|string[]> headers;
|};

public type ContextShowfnrequest record {|
    showfnrequest content;
    map<string|string[]> headers;
|};

public type ContextShowallwithcriteriarequest record {|
    showallwithcriteriarequest content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextAddmultiplefnsresponse record {|
    Addmultiplefnsresponse content;
    map<string|string[]> headers;
|};

public type ContextShowallwithcriteriaresponse record {|
    showallwithcriteriaresponse content;
    map<string|string[]> headers;
|};

public type ContextAdddeveloperfn record {|
    Adddeveloperfn content;
    map<string|string[]> headers;
|};

public type ContextShowfnResponse record {|
    showfnResponse content;
    map<string|string[]> headers;
|};

public type ContextShowallfnsrequest record {|
    showallfnsrequest content;
    map<string|string[]> headers;
|};

public type ContextShowallfnsresponseParams record {|
    showallfnsresponseParams content;
    map<string|string[]> headers;
|};

public type ContextAddmultiplefnsrequest record {|
    Addmultiplefnsrequest content;
    map<string|string[]> headers;
|};

public type ContextDeletefnresponse record {|
    deletefnresponse content;
    map<string|string[]> headers;
|};

public type ContextDeletefnrequest record {|
    deletefnrequest content;
    map<string|string[]> headers;
|};

public type showfnrequest record {|
    string username = "";
|};

public type showallwithcriteriarequest record {|
    string details = "";
|};

public type Addmultiplefnsresponse record {|
    string message = "";
|};

public type showallwithcriteriaresponse record {|
    string[] names = [];
|};

public type Adddeveloperfn record {|
    string repository = "";
    string username = "";
    string fullname = "";
    string email = "";
    string language = "";
    string position = "";
|};

public type Adddeveloper record {|
    string msg = "";
|};

public type showfnResponse record {|
    string message = "";
|};

public type showallfnsrequest record {|
    string[] details = [];
|};

public type showallfnsresponseParams record {|
    string parameters = "";
|};

public type Addmultiplefnsrequest record {|
    string message = "";
|};

public type deletefnresponse record {|
    string message = "";
|};

public type deletefnrequest record {|
    string username = "";
|};

const string ROOT_DESCRIPTOR = "0A1270726F746F636F6C627566662E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22B6010A0E416464646576656C6F706572666E121E0A0A7265706F7369746F7279180120012809520A7265706F7369746F7279121A0A08757365726E616D651802200128095208757365726E616D65121A0A0866756C6C6E616D65180320012809520866756C6C6E616D6512140A05656D61696C1804200128095205656D61696C121A0A086C616E677561676518052001280952086C616E6775616765121A0A08706F736974696F6E1806200128095208706F736974696F6E22200A0C416464646576656C6F70657212100A036D736718012001280952036D736722310A154164646D756C7469706C65666E737265717565737412180A076D65737361676518012001280952076D65737361676522320A164164646D756C7469706C65666E73726573706F6E736512180A076D65737361676518012001280952076D657373616765222D0A0F64656C657465666E72657175657374121A0A08757365726E616D651801200128095208757365726E616D65222C0A1064656C657465666E726573706F6E736512180A076D65737361676518012001280952076D657373616765222B0A0D73686F77666E72657175657374121A0A08757365726E616D651801200128095208757365726E616D65222A0A0E73686F77666E526573706F6E736512180A076D65737361676518012001280952076D657373616765222D0A1173686F77616C6C666E737265717565737412180A0764657461696C73180120032809520764657461696C73223A0A1873686F77616C6C666E73726573706F6E7365506172616D73121E0A0A706172616D6574657273180120012809520A706172616D657465727322360A1A73686F77616C6C7769746863726974657269617265717565737412180A0764657461696C73180120012809520764657461696C7322330A1B73686F77616C6C776974686372697465726961726573706F6E736512140A056E616D657318012003280952056E616D657332FC020A0966756E6374696F6E73123B0A0A6164645F6E65775F666E120F2E416464646576656C6F706572666E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123A0A076164645F666E7312162E4164646D756C7469706C65666E73726571756573741A172E4164646D756C7469706C65666E73726573706F6E736512300A0964656C6574655F666E12102E64656C657465666E726571756573741A112E64656C657465666E726573706F6E7365122C0A0773686F775F666E120E2E73686F77666E726571756573741A0F2E73686F77666E526573706F6E73653001123F0A0C73686F775F616C6C5F666E7312122E73686F77616C6C666E73726571756573741A192E73686F77616C6C666E73726573706F6E7365506172616D73300112550A1673686F775F616C6C5F776974685F6372697465726961121B2E73686F77616C6C776974686372697465726961726571756573741A1C2E73686F77616C6C776974686372697465726961726573706F6E73653001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "protocolbuff.proto": "0A1270726F746F636F6C627566662E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22B6010A0E416464646576656C6F706572666E121E0A0A7265706F7369746F7279180120012809520A7265706F7369746F7279121A0A08757365726E616D651802200128095208757365726E616D65121A0A0866756C6C6E616D65180320012809520866756C6C6E616D6512140A05656D61696C1804200128095205656D61696C121A0A086C616E677561676518052001280952086C616E6775616765121A0A08706F736974696F6E1806200128095208706F736974696F6E22200A0C416464646576656C6F70657212100A036D736718012001280952036D736722310A154164646D756C7469706C65666E737265717565737412180A076D65737361676518012001280952076D65737361676522320A164164646D756C7469706C65666E73726573706F6E736512180A076D65737361676518012001280952076D657373616765222D0A0F64656C657465666E72657175657374121A0A08757365726E616D651801200128095208757365726E616D65222C0A1064656C657465666E726573706F6E736512180A076D65737361676518012001280952076D657373616765222B0A0D73686F77666E72657175657374121A0A08757365726E616D651801200128095208757365726E616D65222A0A0E73686F77666E526573706F6E736512180A076D65737361676518012001280952076D657373616765222D0A1173686F77616C6C666E737265717565737412180A0764657461696C73180120032809520764657461696C73223A0A1873686F77616C6C666E73726573706F6E7365506172616D73121E0A0A706172616D6574657273180120012809520A706172616D657465727322360A1A73686F77616C6C7769746863726974657269617265717565737412180A0764657461696C73180120012809520764657461696C7322330A1B73686F77616C6C776974686372697465726961726573706F6E736512140A056E616D657318012003280952056E616D657332FC020A0966756E6374696F6E73123B0A0A6164645F6E65775F666E120F2E416464646576656C6F706572666E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123A0A076164645F666E7312162E4164646D756C7469706C65666E73726571756573741A172E4164646D756C7469706C65666E73726573706F6E736512300A0964656C6574655F666E12102E64656C657465666E726571756573741A112E64656C657465666E726573706F6E7365122C0A0773686F775F666E120E2E73686F77666E726571756573741A0F2E73686F77666E526573706F6E73653001123F0A0C73686F775F616C6C5F666E7312122E73686F77616C6C666E73726571756573741A192E73686F77616C6C666E73726573706F6E7365506172616D73300112550A1673686F775F616C6C5F776974685F6372697465726961121B2E73686F77616C6C776974686372697465726961726571756573741A1C2E73686F77616C6C776974686372697465726961726573706F6E73653001620670726F746F33"};
}

