import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);
Developer [] developerlist = [];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functions" on ep {


    remote function add_new_fn(Adddeveloperfn value) returns string|error {
        developerlist.push(value);
        return "Successfully added developer :"+(value.username).toString();


    }
    remote function add_fns(Addmultiplefnsrequest value) returns Addmultiplefnsresponse|error {

    }
    remote function delete_fn(deletefnrequest value) returns deletefnresponse|error {
          developerlist.pull(value);
          return " developer successfully deleted:" +(value.fullname);
          

    }
    remote function show_fn(showfnrequest value) returns stream<showfnResponse, error?>|error {
        log:printInfo(value.username);
        showfnResponse res ={display:" value.fullname,value.position,value.language,value.email"};
        return res
    }
    remote function show_all_fns(showallfnsrequest value) returns stream<showallfnsresponseParams, error?>|error {
        showallfnrequest res =(displayallfunctions: " ")
        return res;
   }
    remote function show_all_with_criteria(showallwithcriteriarequest value) returns stream<showallwithcriteriaresponse, error?>|error {
    }
}

