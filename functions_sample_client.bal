import ballerina/io;

functionsClient ep = check new ("http://localhost:9090");

public function main() {
Developer tulinelao ={username:"Tulina",fullname:"Tulina Matheus",email:"azeamatheus@gmail.com",language:"Ballerina",position:"Senior Developer"};

Developer hamata ={username:"Pan",fullname:"Pandu Hamata",email:"panduhamata@gmail.com",language:"Ballerina",position:"Junior Developer"};

//make an  RPC 
response|error outcome = check ep->show_fn(tulinelao);
io:print( " developer function response ",outcome);

response|error outcome2 = check ep->show_fn(stream:"");
io:print( "n\ndeveloper function response ",outcome2);

}
}


