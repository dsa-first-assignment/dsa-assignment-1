import ballerina/io;
import ballerina/http;


//create learner profile
type learnerProfile record {|
string username;
string lastname;
string firstname;
string[] format;
past_subjects[] subjects;
|};

//Learning Materials
type learnerMaterials record {|
string course;
learning_objects learning_objects;
|};

// learning objects required
type learning_objects record {|
required required;
suggested suggested;
|};

//required text and audio
 type required record {|
audio audio;
text text;
|};

type suggested record {|
string[] video;
string[] audio;
|};

type audio record {|
string name;
string description;
string difficulty;
|};

type text record {|
string description;
|};

//preferred format field for learner profile
type past_subjects record {|
string course;
string score;
|};



//string[] format = []; unsure if prior array declaration needed

learnerProfile[] all_learners = [];
learnerMaterials[] all_materials = [];

service /learner on new http:Listener(8080) {


    //get all learner profiles
    resource function get getAllLearners() returns learnerProfile[]{
        io:println("handling get requests to /learner/getAllLearners");
        return all_learners;
    }
    

    //insert new learner profile
      resource function post insert(@http:Payload learnerProfile new_profile) returns json  {
        io:println("handling POST request to /users/insert");
        all_learners.put(new_profile);
        return {done: "Ok"};
    }

    //update User Profile
    resource function put updateLearner(@http:Payload learnerProfile profile) returns json {
             io:println("handling POST request to /users/insert");
        all_learners.push(profile);
    }


}

service /material on new http:Listener(8081) {


    //get all learner profiles
    resource function get getAllMaterials() returns learnerMaterials[]{
        io:println("handling get requests to /learner/getAllLearners");
        return all_materials;
    }
    

    //insert new learner profile
      resource function post insert(@http:Payload learnerMaterials new_materials) returns json  {
        io:println("handling POST request to /users/insert");
        all_materials.push(new_materials);
        return {done: "Ok"};
    }

    //update User Profile
    resource function put updateMaterials(@http:Payload learnerMaterials materials) returns json {
             io:println("handling POST request to /users/insert");
        all_materials.push(materials);
    }


}