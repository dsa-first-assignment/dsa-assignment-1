import  ballerina/http;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service  /v1  on  ep0  {
        resource  function  get  learner()  returns  string[]|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
        resource  function  put  learner(@http:Payload  {} record  {|string  lastname; string  firstname; string  username; []  peffered_format; []  past_subjects;|}  payload)  returns  http:Ok|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
        resource  function  post  learner(@http:Payload  {} record  {|string  lastname; string  firstname; string  username; []  peffered_format; []  past_subjects;|}  payload)  returns  http:Ok|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
        resource  function  get  learner/[string  username]()  returns  record  {|string  lastname; string  firstname; []  peffered_format; []  past_subjects;|}|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
        resource  function  post  learner/[string  username]()  returns  record  {|string  lastname; string  firstname; []  peffered_format; []  past_subjects;|}|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
        resource  function  delete  learner/[string  username]()  returns  record  {|string  lastname; string  firstname; []  peffered_format; []  past_subjects;|}|record  {|*http:BadRequest; record  {|string  message;|}  body;|} {
    }
}
