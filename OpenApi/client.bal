import  ballerina/http;

public type record {}Arr record {}[];

public type PutlearnerRequest record {
    string lastname?;
    string firstname?;
    string username;
    string[] peffered_format?;
    string[][] past_subjects?;
};

public type PostlearnerRequest record {
    string lastname?;
    string firstname?;
    string username;
    string[] peffered_format?;
    string[][] past_subjects?;
};

# Successfully returned learner profile.
public type GetlearnerByusernameResponse record {
    string lastname?;
    string firstname?;
    string[] peffered_format?;
    string[][] past_subjects?;
};

# Successfully updated {username} learner profile.
public type PostlearnerByusernameResponse record {
    string lastname?;
    string firstname?;
    string[] peffered_format?;
    string[][] past_subjects?;
};

# Successfully deleted learner profile.
public type DeletelearnerByusernameResponse record {
    string lastname?;
    string firstname?;
    string[] peffered_format?;
    string[][] past_subjects?;
};

# A simple API to illustrate an Virtual Learning environment for learners
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "https://api.server.learner/v1") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    # + return - Successfully returned a list of learners
    remote isolated function getlearner() returns record {}Arr|error {
        string  path = string `/learner`;
        record {}Arr response = check self.clientEp-> get(path, targetType = record {}Arr);
        return response;
    }
    #
    # + return - Successfully updated Learner profile
    remote isolated function putlearner(PutlearnerRequest payload) returns error? {
        string  path = string `/learner`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> put(path, request, targetType=http:Response);
    }
    #
    # + return - Successfully created a new Learner
    remote isolated function postlearner(PostlearnerRequest payload) returns error? {
        string  path = string `/learner`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
    #
    # + return - Successfully returned learner profile.
    remote isolated function getlearnerByusername(string username) returns GetlearnerByusernameResponse|error {
        string  path = string `/learner/${username}`;
        GetlearnerByusernameResponse response = check self.clientEp-> get(path, targetType = GetlearnerByusernameResponse);
        return response;
    }
    #
    # + return - Successfully updated {username} learner profile.
    remote isolated function postlearnerByusername(string username) returns PostlearnerByusernameResponse|error {
        string  path = string `/learner/${username}`;
        http:Request request = new;
        //TODO: Update the request as needed;
        PostlearnerByusernameResponse response = check self.clientEp-> post(path, request, targetType = PostlearnerByusernameResponse);
        return response;
    }
    #
    # + return - Successfully deleted learner profile.
    remote isolated function deletelearnerByusername(string username) returns DeletelearnerByusernameResponse|error {
        string  path = string `/learner/${username}`;
        http:Request request = new;
        //TODO: Update the request as needed;
        DeletelearnerByusernameResponse response = check self.clientEp-> delete(path, request, targetType = DeletelearnerByusernameResponse);
        return response;
    }
}
